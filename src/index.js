import "./styles/index.scss";
import IMask from 'imask';
import toNextSlide from './modules/toNextSlide';
import cardClick from './modules/cardClick';
import phoneSymbolDelete from './modules/phoneSymbolDelete';
const LIVR = require('livr');
LIVR.Validator.defaultAutoTrim(true);

const buttonToStep2 = document.getElementById('toStep2');
const phoneInput = document.getElementById('userPhone');
const phoneElement = document.getElementById('userPhone');
const formElement = document.getElementById('formOrder');
const cardElements = document.querySelectorAll('.card__item');

buttonToStep2.addEventListener('click', () => {
    toNextSlide(1);
});

cardElements.forEach(cardItem => {
   cardItem.addEventListener('click', cardClick);
});

const phoneMask = IMask(
    phoneInput, {mask: '+{38}(000)000-00-00'}
);

phoneElement.addEventListener('input', () => {
    phoneElement.classList.remove('input--error');
});

formElement.addEventListener("submit", (event)=> {
    event.preventDefault();
    phoneElement.classList.remove('input--error');
    const validator = new LIVR.Validator({
        phone: { min_length: 12 },
    });
    const phoneValue = phoneElement.value;
    const validPhoneValue = phoneSymbolDelete(phoneValue);
    const userData = {};
    userData['phone'] = validPhoneValue;
    const validData = validator.validate(userData);
    if (validData && validPhoneValue !== '') {
        alert(`Спасибо! Ваш намер телефона: +${validPhoneValue}`)
    } else if (validData && validPhoneValue === '') {
        alert('Вы не ввели номер телефона!')
    } else {
        phoneElement.classList.add('input--error');
        console.log('errors', validator.getErrors());
    }
});