const toNextSlide = (currentSlide) => {
    const stepsWrapper = document.querySelector('.project__steps');
    let step = 100*currentSlide;
    stepsWrapper.style.marginLeft=`-${step}vw`;
};

export default toNextSlide;