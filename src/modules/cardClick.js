import toNextSlide from './toNextSlide';

const projectCostAdd = (costElement) => {
    const cost = sessionStorage.getItem('projectCost');
    if(cost) {
        costElement.innerHTML = `&nbsp;${cost}$`;
    }
};

const cardClick = (event) => {
  const target = event.target;
  const projectCostElement = document.querySelector('.project__cost');
  let projectCost = target.getAttribute('data-cost');
  sessionStorage.setItem('projectCost', projectCost);
  projectCostAdd(projectCostElement);
  toNextSlide(2);
};

export default cardClick;